object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 450
  ClientWidth = 773
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 773
    Height = 430
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet3
    Properties.Rotate = True
    Properties.TabPosition = tpLeft
    ClientRectBottom = 426
    ClientRectLeft = 84
    ClientRectRight = 769
    ClientRectTop = 4
    object cxTabSheet1: TcxTabSheet
      Caption = 'Konektovanje'
      ImageIndex = 0
      object cxLabel1: TcxLabel
        Left = 24
        Top = 16
        Caption = 'IP Adresa'
      end
      object edtIP: TcxTextEdit
        Left = 88
        Top = 16
        TabOrder = 1
        Text = 'edtIP'
        Width = 121
      end
      object edtPort: TcxTextEdit
        Left = 88
        Top = 40
        TabOrder = 2
        Text = 'edtPort'
        Width = 121
      end
      object btnConnect: TcxButton
        Left = 48
        Top = 72
        Width = 75
        Height = 25
        Caption = 'Connect'
        TabOrder = 3
        OnClick = btnConnectClick
      end
      object btnDisconnect: TcxButton
        Left = 136
        Top = 72
        Width = 75
        Height = 25
        Caption = 'Disconnect'
        TabOrder = 4
        OnClick = btnDisconnectClick
      end
      object cxLabel2: TcxLabel
        Left = 24
        Top = 40
        Caption = 'Port'
      end
      object cxGroupBox1: TcxGroupBox
        Left = 24
        Top = 112
        Caption = 'Informacije o ure'#273'aju'
        TabOrder = 6
        Height = 137
        Width = 297
        object cxLabel3: TcxLabel
          Left = 16
          Top = 24
          Caption = 'Device number:'
        end
        object cxLabel4: TcxLabel
          Left = 16
          Top = 48
          Caption = 'Number of administrators:'
        end
        object lblDeviceNumber: TcxLabel
          Left = 144
          Top = 24
          Caption = 'lblDeviceNumber'
        end
        object lblNoOfAdmins: TcxLabel
          Left = 144
          Top = 48
          Caption = 'lblNoOfAdmins'
        end
        object cxLabel5: TcxLabel
          Left = 16
          Top = 72
          Caption = 'Number of users:'
        end
        object lblNoOfUsers: TcxLabel
          Left = 144
          Top = 72
          Caption = 'lblNoOfUsers'
        end
        object cxLabel6: TcxLabel
          Left = 16
          Top = 96
          Caption = 'Number of attendances:'
        end
        object lblNoOfAttendances: TcxLabel
          Left = 144
          Top = 96
          Caption = 'lblNoOfAttendances'
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Korisnici'
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 685
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnShowUsers: TcxButton
          Left = 8
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Prika'#382'i'
          TabOrder = 0
          OnClick = btnShowUsersClick
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 41
        Width = 685
        Height = 381
        Align = alClient
        DataSource = dsKorisnici
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'MachineNumber'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EnrollNumber'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Name'
            Width = 134
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Password'
            Width = 142
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Privilege'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Enabled'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CardNumber'
            Width = 92
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TZs'
            Width = 95
            Visible = True
          end>
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Pristupi'
      ImageIndex = 2
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 685
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnShowAttendances: TcxButton
          Left = 8
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Prika'#382'i'
          TabOrder = 0
          OnClick = btnShowAttendancesClick
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 41
        Width = 685
        Height = 381
        Align = alClient
        DataSource = dscdsPristupi
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'TMachineNumber'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EnrollNumber'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VerifyMode'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'InOutMode'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Year'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Month'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Day'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Hour'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Minute'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Second'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WorkCode'
            Visible = True
          end>
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Monitor'
      ImageIndex = 3
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 685
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnStartMonitor: TcxButton
          Left = 15
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Start'
          TabOrder = 0
          OnClick = btnStartMonitorClick
        end
        object btnStopMonitor: TcxButton
          Left = 96
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Stop'
          TabOrder = 1
          OnClick = btnStopMonitorClick
        end
      end
      object memMonitor: TMemo
        Left = 0
        Top = 41
        Width = 685
        Height = 381
        Align = alClient
        TabOrder = 1
      end
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 430
    Width = 773
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cdsKorisnici: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 56
    Data = {
      D40000009619E0BD010000001800000008000000000003000000D4000D4D6163
      68696E654E756D62657204000100000000000C456E726F6C6C4E756D62657204
      00010000000000044E616D650100490000000100055749445448020002004000
      0850617373776F72640100490000000100055749445448020002004000095072
      6976696C656765040001000000000007456E61626C656402000300000000000A
      436172644E756D6265720100490000000100055749445448020002001E000354
      5A7301004900000001000557494454480200020032000000}
    object cdsKorisniciMachineNumber: TIntegerField
      FieldName = 'MachineNumber'
    end
    object cdsKorisniciEnrollNumber: TIntegerField
      FieldName = 'EnrollNumber'
    end
    object cdsKorisniciName: TStringField
      FieldName = 'Name'
      Size = 64
    end
    object cdsKorisniciPassword: TStringField
      FieldName = 'Password'
      Size = 64
    end
    object cdsKorisniciPrivilege: TIntegerField
      FieldName = 'Privilege'
    end
    object cdsKorisniciEnabled: TBooleanField
      FieldName = 'Enabled'
    end
    object cdsKorisniciCardNumber: TStringField
      FieldName = 'CardNumber'
      Size = 30
    end
    object cdsKorisniciTZs: TStringField
      FieldName = 'TZs'
      Size = 50
    end
  end
  object dsKorisnici: TDataSource
    DataSet = cdsKorisnici
    Left = 176
    Top = 56
  end
  object cdsPristupi: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'TMachineNumber'
        DataType = ftInteger
      end
      item
        Name = 'EnrollNumber'
        DataType = ftInteger
      end
      item
        Name = 'VerifyMode'
        DataType = ftInteger
      end
      item
        Name = 'InOutMode'
        DataType = ftInteger
      end
      item
        Name = 'Year'
        DataType = ftInteger
      end
      item
        Name = 'Month'
        DataType = ftInteger
      end
      item
        Name = 'Day'
        DataType = ftInteger
      end
      item
        Name = 'Hour'
        DataType = ftInteger
      end
      item
        Name = 'Minute'
        DataType = ftInteger
      end
      item
        Name = 'Second'
        DataType = ftInteger
      end
      item
        Name = 'WorkCode'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 120
    Top = 112
    Data = {
      CE0000009619E0BD01000000180000000B000000000003000000CE000E544D61
      6368696E654E756D62657204000100000000000C456E726F6C6C4E756D626572
      04000100000000000A5665726966794D6F6465040001000000000009496E4F75
      744D6F6465040001000000000004596561720400010000000000054D6F6E7468
      040001000000000003446179040001000000000004486F757204000100000000
      00064D696E7574650400010000000000065365636F6E64040001000000000008
      576F726B436F646504000100000000000000}
    object cdsPristupiEnrollNumber: TIntegerField
      FieldName = 'EnrollNumber'
    end
    object cdsPristupiVerifyMode: TIntegerField
      FieldName = 'VerifyMode'
    end
    object cdsPristupiInOutMode: TIntegerField
      FieldName = 'InOutMode'
    end
    object cdsPristupiYear: TIntegerField
      FieldName = 'Year'
    end
    object cdsPristupiMonth: TIntegerField
      FieldName = 'Month'
    end
    object cdsPristupiDay: TIntegerField
      FieldName = 'Day'
    end
    object cdsPristupiHour: TIntegerField
      FieldName = 'Hour'
    end
    object cdsPristupiMinute: TIntegerField
      FieldName = 'Minute'
    end
    object cdsPristupiSecond: TIntegerField
      FieldName = 'Second'
    end
    object cdsPristupiWorkCode: TIntegerField
      FieldName = 'WorkCode'
    end
  end
  object dscdsPristupi: TDataSource
    DataSet = cdsPristupi
    Left = 176
    Top = 112
  end
end
