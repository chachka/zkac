unit uMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, zkemkeeper_TLB, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxPCdxBarPopupMenu,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxTextEdit, cxLabel, cxPC, dxStatusBar, cxGroupBox, ExtCtrls, Grids,
  DBGrids, DB, DBClient;

type
  TMainForm = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxLabel1: TcxLabel;
    edtIP: TcxTextEdit;
    edtPort: TcxTextEdit;
    btnConnect: TcxButton;
    btnDisconnect: TcxButton;
    cxLabel2: TcxLabel;
    StatusBar: TdxStatusBar;
    cxGroupBox1: TcxGroupBox;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    lblDeviceNumber: TcxLabel;
    lblNoOfAdmins: TcxLabel;
    cxLabel5: TcxLabel;
    lblNoOfUsers: TcxLabel;
    cxLabel6: TcxLabel;
    lblNoOfAttendances: TcxLabel;
    cxTabSheet2: TcxTabSheet;
    Panel1: TPanel;
    cdsKorisnici: TClientDataSet;
    dsKorisnici: TDataSource;
    DBGrid1: TDBGrid;
    btnShowUsers: TcxButton;
    cdsKorisniciMachineNumber: TIntegerField;
    cdsKorisniciEnrollNumber: TIntegerField;
    cdsKorisniciName: TStringField;
    cdsKorisniciPassword: TStringField;
    cdsKorisniciPrivilege: TIntegerField;
    cdsKorisniciEnabled: TBooleanField;
    cdsKorisniciCardNumber: TStringField;
    cdsKorisniciTZs: TStringField;
    cxTabSheet3: TcxTabSheet;
    Panel2: TPanel;
    btnShowAttendances: TcxButton;
    cdsPristupi: TClientDataSet;
    dscdsPristupi: TDataSource;
    DBGrid2: TDBGrid;
    cdsPristupiEnrollNumber: TIntegerField;
    cdsPristupiVerifyMode: TIntegerField;
    cdsPristupiInOutMode: TIntegerField;
    cdsPristupiYear: TIntegerField;
    cdsPristupiMonth: TIntegerField;
    cdsPristupiDay: TIntegerField;
    cdsPristupiHour: TIntegerField;
    cdsPristupiMinute: TIntegerField;
    cxTabSheet4: TcxTabSheet;
    Panel3: TPanel;
    btnStartMonitor: TcxButton;
    btnStopMonitor: TcxButton;
    memMonitor: TMemo;
    cdsPristupiSecond: TIntegerField;
    cdsPristupiWorkCode: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure btnDisconnectClick(Sender: TObject);
    procedure btnShowUsersClick(Sender: TObject);
    procedure btnShowAttendancesClick(Sender: TObject);
    procedure btnStartMonitorClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnStopMonitorClick(Sender: TObject);
  private
    ZKem: TCZKem;
    procedure PrikaziInfoOUredjaju();
    procedure OnAttTransaction(ASender: TObject; EnrollNumber: Integer; IsInValid: Integer;
                                                       AttState: Integer; VerifyMethod: Integer;
                                                       Year: Integer; Month: Integer; Day: Integer;
                                                       Hour: Integer; Minute: Integer;
                                                       Second: Integer);
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btnConnectClick(Sender: TObject);
  var
    ErrorCode: Integer;
begin
  if ZKem.Connect_Net(edtIP.Text, StrToInt(edtPort.Text)) then begin
    StatusBar.Panels[0].Text := 'Spojen na ure�aj: ' + edtIP.Text;
    PrikaziInfoOUredjaju();
  end else begin
    ZKem.GetLastError(ErrorCode);
    StatusBar.Panels[0].Text := 'Gre�ka ' + IntToStr(ErrorCode) + ' pri spajanju na ure�aj: ' + edtIP.Text;
  end;
end;

procedure TMainForm.btnDisconnectClick(Sender: TObject);
begin
  ZKem.Disconnect;
  StatusBar.Panels[0].Text := '';
  cdsKorisnici.LogChanges := False;
end;

procedure TMainForm.btnShowAttendancesClick(Sender: TObject);
  var
    EnrollNumber, VerifyMode, InOutMode, Year, Month, Day, Hour, Minute, Second, WorkCode, Reserved: Integer;
begin
  if ZKem.ReadGeneralLogData(1) then begin
    while ZKem.GetGeneralExtLogData(1, EnrollNumber, VerifyMode, InOutMode, Year, Month, Day, Hour, Minute, Second, WorkCode, Reserved) do begin
      cdsPristupi.Append;
      cdsPristupiEnrollNumber.AsInteger := EnrollNumber;
      cdsPristupiVerifyMode.AsInteger := VerifyMode;
      cdsPristupiInOutMode.AsInteger := InOutMode;
      cdsPristupiYear.AsInteger := Year;
      cdsPristupiMonth.AsInteger := Month;
      cdsPristupiDay.AsInteger := Day;
      cdsPristupiHour.AsInteger := Hour;
      cdsPristupiMinute.AsInteger := Minute;
      cdsPristupiSecond.AsInteger := Second;
      cdsPristupiWorkCode.AsInteger := WorkCode;
      cdsPristupi.Post;
    end;
  end;
end;

procedure TMainForm.btnShowUsersClick(Sender: TObject);
  var
    MachineNumber, EnrollNumber, Privilege: Integer;
    Name, Password, CardNumber: WideString;
    Enabled: WordBool;
    TZs: WideString;
begin
  if ZKem.ReadAllUserID(1) then begin
    while ZKem.GetAllUserInfo(MachineNumber, EnrollNumber, Name, Password, Privilege, Enabled) do begin
      ZKem.GetStrCardNumber(CardNumber);
      ZKem.GetUserTZStr(1, EnrollNumber, TZs);
      cdsKorisnici.Append;
      cdsKorisniciMachineNumber.AsInteger := MachineNumber;
      cdsKorisniciEnrollNumber.AsInteger := EnrollNumber;
      cdsKorisniciName.AsWideString := Name;
      cdsKorisniciPassword.AsWideString := Password;
      cdsKorisniciPrivilege.AsInteger := Privilege;
      cdsKorisniciEnabled.AsBoolean := Enabled;
      cdsKorisniciCardNumber.AsWideString := CardNumber;
      cdsKorisniciTZs.AsWideString := TZs;
      cdsKorisnici.Post;
    end;
  end;
end;

procedure TMainForm.btnStartMonitorClick(Sender: TObject);
begin
  ZKem.RegEvent(1, 1); // 1 = OnAttTransaction
  ZKem.OnAttTransaction := OnAttTransaction;
end;

procedure TMainForm.btnStopMonitorClick(Sender: TObject);
begin
  ZKem.RegEvent(1, 0);
  ZKem.OnAttTransaction := nil;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ZKem.OnAttTransaction := nil;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  ZKem := TCZKEM.Create(Application);
  edtIp.Text := '192.168.0.99';
  edtPort.Text := '4370';
  lblDeviceNumber.Caption := '';
  lblNoOfAdmins.Caption := '';
  lblNoOfUsers.Caption := '';
  lblNoOfAttendances.Caption := '';
end;

procedure TMainForm.OnAttTransaction(ASender: TObject; EnrollNumber, IsInValid, AttState, VerifyMethod, Year, Month, Day, Hour, Minute, Second: Integer);
begin
  memMonitor.Lines.Add(IntToStr(EnrollNumber));
end;

procedure TMainForm.PrikaziInfoOUredjaju;
  var
    MachineNumber, Info, Value: Integer;
begin
  if ZKem.GetDeviceInfo(1, 2, Value) then
    lblDeviceNumber.Caption := IntToStr(Value);
  if ZKem.GetDeviceStatus(1, 1, Value) then
    lblNoOfAdmins.Caption := IntToStr(Value);
  if ZKem.GetDeviceStatus(1, 2, Value) then
    lblNoOfUsers.Caption := IntToStr(Value);
  if ZKem.GetDeviceStatus(1, 6, Value) then
    lblNoOfAttendances.Caption := IntToStr(Value);
  if ZKem.GetDeviceStatus(1, 8, Value) then
    lblNoOfUsers.Caption := lblNoOfUsers.Caption + ' / ' + IntToStr(Value);
  if ZKem.GetDeviceStatus(1, 9, Value) then
    lblNoOfAttendances.Caption := lblNoOfAttendances.Caption + ' / ' + IntToStr(Value);
end;

end.
