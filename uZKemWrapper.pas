unit uZKemWrapper;

interface

uses
  zkemkeeper_TLB;

const
  ZKUP_COMMON_USER = 0;
  ZKUP_ENROLLER = 1;
  ZKUP_ADMINISTRATOR = 2;
  ZKUP_SUPER_ADMINISTRATOR = 3;

type

  TZKUser = record
    EnrollNumber: Integer;
    Name: WideString;
    Password: WideString;
    Privilege: Integer;
    Enabled: WordBool;
    CardNumber: WideString;
    TZs: WideString;
    IsUsingGroupTimeZone: WordBool;
    UserGrp: Integer;
  end;

  TZKUserGroups = record
    GroupIndex: Integer;
    TZs: WideString;
  end;

  TZKAttendance = record
    EnrollNumber: Integer;
    VerifyMode: Integer;
    InOutMode: Integer;
    Year: Integer;
    Month: Integer;
    Day: Integer;
    Hour: Integer;
    Minute: Integer;
    Second: Integer;
    WorkCode: Integer;
    Reserved: Integer;
  end;

  TZKTimeZones = record
    TZIndex: Integer;
    Sunday: WideString;
    Monday: WideString;
    Tuesday: WideString;
    Wednesday: WideString;
    Thursday: WideString;
    Friday: WideString;
    Saturday: WideString;
  end;

  TZKDevice = class

  private
    fZKem: TCZKem;
    fIPAdd: WideString;
    fPort: Integer;
    fIsConnected: Boolean;
    fUser: TZKUser;
    fAttendance: TZKAttendance;
    fTimeZone: TZKTimeZones;
    fUserGroup: TZKUserGroups;
    function GetLastErrorCode: Integer;

  public
    constructor Create(const aIPAdd: WideString; const aPort: Integer = 4370);
    destructor Destroy; override;

  public
    property ErrorCode: Integer read GetLastErrorCode;

  public
    procedure Connect;
    procedure Disconnect;
    property IsConnected: Boolean read fIsConnected;
    function Enable: Boolean;
    function Disable: Boolean;

  public
    function LoadAllUsers: Boolean;
    function GetUser: Boolean;
    property User: TZKUser read fUser;
    function SaveUser: Boolean;

    function ClearAllUsers: Boolean;
    function EnableUser(aEnrollNumber: Integer): Boolean;
    function DisableUser(aEnrollNumber: Integer): Boolean;

  public
    function LoadAllAttendances: Boolean;
    function GetAttendance: Boolean;
    property Attendance: TZKAttendance read fAttendance;

    function ClearAllAttendances: Boolean;

  public
    property TimeZone: TZKTimeZones read fTimeZone;
    function GetTimeZone(const aTimeZoneIndex: Integer): Boolean;
    function SetTimeZone: Boolean;

  public
    property UserGroup: TZKUserGroups read fUserGroup;
    function GetGroupTimeZone(const aGroupIndex: Integer): Boolean;
    function SetGroupTimeZone: Boolean;

  end;

implementation

{ TZKDevice }

function TZKDevice.ClearAllAttendances: Boolean;
begin
  Result := fZKem.ClearGLog(1);
end;

function TZKDevice.ClearAllUsers: Boolean;
begin
  fZKem.ClearData(1, 5);
end;

procedure TZKDevice.Connect;
begin
  fIsConnected := fZKem.Connect_Net(fIPAdd, fPort);
end;

constructor TZKDevice.Create(const aIPAdd: WideString; const aPort: Integer);
begin
  inherited Create;
  fIPAdd := aIPAdd;
  fPort := aPort;
end;

destructor TZKDevice.Destroy;
begin
  fZKem.Destroy;
  inherited;
end;

function TZKDevice.Disable: Boolean;
begin
  Result := fZKem.EnableDevice(1, False);
end;

function TZKDevice.DisableUser(aEnrollNumber: Integer): Boolean;
begin
  Result := fZKem.EnableUser(1, aEnrollNumber, 1, 1, True);
end;

procedure TZKDevice.Disconnect;
begin
  try
    fZKem.Disconnect;
  finally
    fIsConnected := False;
  end;
end;

function TZKDevice.Enable: Boolean;
begin
  Result := fZKem.EnableDevice(1, Disable);
end;

function TZKDevice.EnableUser(aEnrollNumber: Integer): Boolean;
begin
  Result := fZKem.EnableUser(1, aEnrollNumber, 1, 1, False);
end;

function TZKDevice.GetUser: Boolean;
begin
  Result := False;
  if fZKem.GetAllUserInfo(1, fUser.EnrollNumber, fUser.Name, fUser.Password, fUser.Privilege, fUser.Enabled) then begin
    fZKem.GetStrCardNumber(fUser.CardNumber);
    fZKem.GetUserTZStr(1, User.EnrollNumber, fUser.TZs);
    fUser.IsUsingGroupTimeZone := fZKem.UseGroupTimeZone();
    fZKem.GetUserGroup(1, User.EnrollNumber, fUser.UserGrp);
    Result := True;
  end;
end;

function TZKDevice.GetAttendance: Boolean;
begin
  Result := fZKem.GetGeneralExtLogData(1, fAttendance.EnrollNumber, fAttendance.VerifyMode, fAttendance.InOutMode, fAttendance.Year, fAttendance.Month, fAttendance.Day, fAttendance.Hour, fAttendance.Minute, fAttendance.Second, fAttendance.WorkCode, fAttendance.Reserved);
end;

function TZKDevice.GetGroupTimeZone(const aGroupIndex: Integer): Boolean;
begin
  Result := fZKem.GetGroupTZStr(1, aGroupIndex, fUserGroup.TZs);
  if Result then
    fUserGroup.GroupIndex := aGroupIndex;
end;

function TZKDevice.GetLastErrorCode: Integer;
begin
  fZKem.GetLastError(Result);
end;

function TZKDevice.GetTimeZone(const aTimeZoneIndex: Integer): Boolean;
  var
    lTZ: WideString;
begin
  Result := fZKem.GetTZInfo(1, aTimeZoneIndex, lTZ);
  if Result then begin
    fTimeZone.TZIndex := aTimeZoneIndex;
    fTimeZone.Sunday := Copy(lTZ, 1, 8);
    fTimeZone.Monday := Copy(lTZ, 9, 8);
    fTimeZone.Tuesday := Copy(lTZ, 17, 8);
    fTimeZone.Wednesday := Copy(lTZ, 25, 8);
    fTimeZone.Thursday := Copy(lTZ, 33, 8);
    fTimeZone.Friday := Copy(lTZ, 41, 8);
    fTimeZone.Saturday := Copy(lTZ, 49, 8);
  end;
end;

function TZKDevice.LoadAllAttendances: Boolean;
begin
  Result := fZKem.ReadGeneralLogData(1);
end;

function TZKDevice.LoadAllUsers: Boolean;
begin
  Result := fZKem.ReadAllUserID(1);
end;

function TZKDevice.SaveUser: Boolean;
begin
  fZKem.SetStrCardNumber(User.CardNumber);
  Result := fZKem.SetUserInfoEx(1, User.EnrollNumber, User.Name, User.Password, User.Privilege, User.Enabled);
  if Result then begin
    Result := fZKem.SetUserGroup(1, User.EnrollNumber, User.UserGrp);
    if Result and not User.IsUsingGroupTimeZone then
      Result := fZKem.SetUserTZStr(1, User.EnrollNumber, User.TZs);
  end;
end;

function TZKDevice.SetGroupTimeZone: Boolean;
begin
  Result := fZKem.SetGroupTZStr(1, UserGroup.GroupIndex, UserGroup.TZs);
end;

function TZKDevice.SetTimeZone: Boolean;
begin
  Result := fZKem.SetTZInfo(1, TimeZone.TZIndex, TimeZone.Sunday + TimeZone.Monday + TimeZone.Tuesday + TimeZone.Wednesday + TimeZone.Thursday + TimeZone.Friday + TimeZone.Saturday);
end;

end.
